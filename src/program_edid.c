/*
 * (C) Copyright 2017
 * Olliver Schinagl <o.schinagl@ultimaker.com>
 *
 * SPX-License-Identifier:	AGPL-3.0+
 *
 */

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "version.h"

#define EDID_ADDR		0x50 /* Default EDID chip address. */
#define EDID_ADDR_MAX		0x7f /* Top I2C addr EDID chip address. */
#define EDID_DESC_SIZE		128 /* The EDID descriptor is exactly 128 bytes */

#define EDID_MF_PCODE_OFFSET	10
#define EDID_SERIAL_OFFSET	12
#define EDID_MF_WEEK_OFFSET	16
#define EDID_MF_YEAR_OFFSET	17
#define EDID_NO_EXT_OFFSET	126
#define EDID_CHECKSUM_OFFSET	127

#define I2C_BUS_COUNT_MAX	127
#define I2C_WRITE_TIMEOUT	25 * 1000 * 1000 /* 25 milliseconds */

#if I2C_WRITE_TIMEOUT >= INT64_MAX
#error "I2C_WRITE_TIMEOUT too large"
#endif

#define WEEKS_PER_YEAR		54
#define EDID_YEAR_OFFSET	1990
#define EDID_YEAR_MAX		2245

struct manufacturer_data {
	uint16_t product_code;
	uint32_t serial;
	uint8_t week;
	uint16_t year;
};

struct eeprom_data {
	uint8_t chip_addr;
	uint8_t i2c_bus;
};

void version(char *argv0)
{
	fprintf(stderr, "%s %s\n", argv0, EDID_VERSION);
}

void help(char *argv0)
{
	fprintf(stderr, "Usage: %s [OPTION]... FILE\n"
			"\n"
			"  -b, --i2c-bus=BUS	I2C bus number to send data to (/dev/i2c-n)\n"
			"  -a, --chip-addr=ADDR	eeprom address (defaults to 0x%02x)\n"
			"  -p, --prod-code=code	product code (required)\n"
			"  -s, --serial=serial	unique serial number (required)\n"
			"  -w, --week=week	week of manufacture (required)\n"
			"  -y, --year=year	year of manufacture (required)\n"
			"  -h, --help		this help screen\n"
			"      --version	software version number\n"
			"\n"
			"  FILE containing valid eeprom data\n"
			"\n",
			argv0, EDID_ADDR);
}

int64_t timespec_cmp_ns(struct timespec *end, struct timespec *start)
{
	int64_t nsec_start, nsec_end;

	if (end->tv_sec < start->tv_sec)
		return INT64_MAX;

	if (end->tv_sec == start->tv_sec) {
		return end->tv_nsec - start->tv_nsec;
	}

	if ((end->tv_sec > UINT32_MAX) || (start->tv_sec > UINT32_MAX))
		return INT64_MAX;

	nsec_start = (start->tv_sec * 1000 * 1000 * 1000) + start->tv_nsec;
	nsec_end = (end->tv_sec * 1000 * 1000 * 1000) + end->tv_nsec;

	return nsec_end - nsec_start;
}

int i2c_write(int i2c_dev, uint8_t addr, uint8_t data)
{
	uint8_t buf[] = { addr, data };
	struct timespec start, now;
	int timeout = 0;
	int ret;

	errno = 0;

	ret = clock_gettime(CLOCK_MONOTONIC, &start);
	if (ret) {
		fprintf(stderr, "Failed to get start time: %s\n", strerror(errno));
		return errno;
	}
	ret = write(i2c_dev, buf, sizeof(buf));
	if (ret < 0) {
		fprintf(stderr, "Failed to write to byte 0x%02x: %s\n", addr, strerror(errno));
	}

	do {
		ret = write(i2c_dev, &addr, sizeof(addr));
		if (ret == sizeof(addr)) {
			ret = read(i2c_dev, &buf[0], sizeof(buf[0]));
			if (ret == sizeof(buf[0]))
				break;
		}

		ret = clock_gettime(CLOCK_MONOTONIC, &now);
		if (ret) {
			fprintf(stderr, "Failed to get current time: %s\n", strerror(errno));
			return errno;
		}
		if ((timespec_cmp_ns(&now, &start)) > I2C_WRITE_TIMEOUT)
			timeout = !0;
	} while (!timeout);

	if (timeout) {
		fprintf(stderr, "Timeout while trying to read from address 0x%02x\n", addr);

		return -ETIMEDOUT;
	}

	if (buf[0] != data) {
		fprintf(stderr, "Write failed, data mismatch: 0x%02x != 0x%02x\n", buf[0], data);
		return -EINVAL;
	}

	return 0;
}

uint8_t checksum_calc(uint8_t *edid)
{
	uint8_t i;
	uint8_t sum;

	if (!edid)
		return -EINVAL;

	sum = 0;
	for (i = 0; i <= EDID_CHECKSUM_OFFSET; i++)
		sum = (sum + edid[i]) % (UINT8_MAX + 1);

	return (UINT8_MAX + 1) - sum;
}

int checksum_verify(uint8_t *edid)
{
	uint8_t checksum;

	checksum = checksum_calc(edid);
	if (checksum != 0x00) {
		fprintf(stderr, "Checksum bad (0x%02x != 0x00)\n", checksum);
		return -EINVAL;
	}

	return 0;
}

int checksum_set(uint8_t *edid)
{
	uint8_t checksum;

	edid[EDID_CHECKSUM_OFFSET] = 0x00;
	checksum = checksum_calc(edid);
	edid[EDID_CHECKSUM_OFFSET] = checksum;

	return checksum_verify(edid);
}

int i2c_read(int i2c_dev, uint8_t addr, uint8_t *data)
{
	int ret;

	errno = 0;

	ret = write(i2c_dev, &addr, sizeof(addr));
	if (ret != sizeof(addr)) {
		fprintf(stderr, "Error setting i2c read address 0x%02x: %s\n", addr, strerror(errno));
		return errno;
	}

	ret = read(i2c_dev, data, sizeof(*data));
	if (ret < 0) {
		fprintf(stderr, "Unable to read data from chip addr 0x%02x: %s\n", addr, strerror(errno));
		return errno;
	}

	return 0;
}

int i2c_slave_set(uint8_t i2c_dev, uint8_t chip_addr)
{
	int ret;

	ret = ioctl(i2c_dev, I2C_SLAVE_FORCE, chip_addr);
	if (ret < 0) {
		fprintf(stderr, "Unable to set slave address to 0x%02x: %s\n", chip_addr, strerror(errno));
		return errno;
	}


	return 0;
}

int i2c_check_funcs(uint8_t i2c_dev)
{
	unsigned long int i2c_funcs;
	int ret;

	errno = 0;

	ret = ioctl(i2c_dev, I2C_FUNCS, &i2c_funcs);
	if (ret < 0) {
		fprintf(stderr, "Unable to ioctl I2C_FUNCS: %s\n", strerror(errno));
		return errno;
	}

	if (!(i2c_funcs & I2C_FUNC_I2C)) {
		fprintf(stderr, "Adapter does not support FUNC_I2C commands\n");
		return -EPROTO;
	}

	return 0;
}

#define I2C_PATH_LEN 15
int i2c_open(uint8_t i2c_bus)
{
	char i2c_path[I2C_PATH_LEN + 1];
	int i2c_dev;

	errno = 0;

	snprintf(i2c_path, I2C_PATH_LEN, "/dev/i2c-%u", i2c_bus);
	i2c_dev = open(i2c_path, O_RDWR);
	if ((i2c_dev < 0) || errno) {
		fprintf(stderr, "Unable to open i2c bus '/dev/i2c-%u': %s\n", i2c_bus, strerror(i2c_bus));
		snprintf(i2c_path, I2C_PATH_LEN, "/dev/i2c/%u", i2c_bus);
		errno = 0;
		i2c_dev = open(i2c_path, O_RDWR);
		if ((i2c_dev < 0) || errno) {
			fprintf(stderr, "Unable to open i2c bus '/dev/i2c/%u': %s\n", i2c_bus, strerror(i2c_bus));
			return errno;
		}
	}

	return i2c_dev;
}

int edid_verify(int i2c_dev, uint8_t *edid, ssize_t size)
{
	uint8_t i;

	fprintf(stderr, "Verifying EDID\n");
	for (i = 0; i < size; i++) {
		int ret;
		uint8_t data;

		ret = i2c_read(i2c_dev, i, &data);
		if (ret < 0)
			return ret;

		if (data != edid[i])
			return -ERANGE;

		fputc('.', stderr);
	}
	fprintf(stderr, "\nVerify complete\n");

	return 0;
}

int edid_write(int i2c_dev, uint8_t *edid, ssize_t size)
{
	uint8_t i;

	fprintf(stderr, "Writing EDID\n");
	for (i = 0;i < size; i++) {
		int ret;

		ret = i2c_write(i2c_dev, i, edid[i]);
		if (ret < 0)
			return ret;

		fputc('.', stderr);
	}
	fprintf(stderr, "\nWrite complete\n");

	return 0;
}

int edid_read(int fd, uint8_t *edid, ssize_t size)
{
	int ret;

	ret = read(fd, edid, size);
	if (ret < 0) {
		fprintf(stderr, "Unable to read edid data: %s\n", strerror(errno));
		return errno;
	}

	return 0;
}

int edid_open(char *filename)
{
	int ret;

	ret = open(filename, O_RDONLY);
	if (ret < 0) {
		fprintf(stderr, "Error opening EDID: %s\n", strerror(errno));
		return errno;
	}

	return ret;
}

void edid_update(uint8_t *edid, const struct manufacturer_data *mfd)
{
	edid[EDID_MF_PCODE_OFFSET + 1] = (mfd->product_code >> 8) & UINT8_MAX;
	edid[EDID_MF_PCODE_OFFSET + 0] = (mfd->product_code >> 0) & UINT8_MAX;

	edid[EDID_SERIAL_OFFSET + 3] = (mfd->serial >> 24) & UINT8_MAX;
	edid[EDID_SERIAL_OFFSET + 2] = (mfd->serial >> 16) & UINT8_MAX;
	edid[EDID_SERIAL_OFFSET + 1] = (mfd->serial >> 8) & UINT8_MAX;
	edid[EDID_SERIAL_OFFSET + 0] = (mfd->serial >> 0) & UINT8_MAX;

	edid[EDID_MF_WEEK_OFFSET] = mfd->week;

	edid[EDID_MF_YEAR_OFFSET] = (mfd->year - EDID_YEAR_OFFSET) & UINT8_MAX;
}

int edid_program(char *edid_filename, struct manufacturer_data *mfd,
		 struct eeprom_data *eeprom)
{
	struct stat edid_stat;
	ssize_t edid_size;
	uint8_t *edid;
	int edid_fd;
	int i2c_dev;
	int ret;
	int i;

	if (!edid_filename)
		return -ENODEV;

	i2c_dev = i2c_open(eeprom->i2c_bus);
	if (i2c_dev < 0) {
		fprintf(stderr, "Open i2c bus %u failed\n", eeprom->i2c_bus);
		return i2c_dev;
	}

	ret = i2c_check_funcs(i2c_dev);
	if (ret < 0) {
		fprintf(stderr, "I2C function check i2c bus %u failed\n",
				eeprom->i2c_bus);

		goto err_dev;
	}

	ret = i2c_slave_set(i2c_dev, eeprom->chip_addr);
	if (ret < 0) {
		fprintf(stderr, "Slave set 0x%02x on i2c bus %u failed\n",
				eeprom->chip_addr, eeprom->i2c_bus);

		goto err_dev;
	}

	edid_fd = edid_open(edid_filename);
	if (edid_fd < 0) {
		fprintf(stderr, "Unable to open edid file '%s'\n", edid_filename);
		ret = edid_fd;

		goto err_dev;
	}

	ret = fstat(edid_fd, &edid_stat);
	if (ret < 0) {
		fprintf(stderr, "Unable to get EDID file stats: %s\n", strerror(errno));

		goto err_fd;
	}

	if (edid_stat.st_size < EDID_DESC_SIZE) {
		fprintf(stderr, "EDID file to small: %jd < %u\n",
				(intmax_t)edid_stat.st_size, EDID_DESC_SIZE);
		ret = -EFAULT;

		goto err_fd;
	}

	edid = (uint8_t *)malloc(edid_stat.st_size * sizeof(uint8_t));
	if (!edid) {
		fprintf(stderr, "Unable to alloc edid memory: %s\n", strerror(errno));
		ret = -ENOMEM;

		goto err_fd;
	}

	ret = edid_read(edid_fd, edid, edid_stat.st_size);
	if (ret < 0) {
		fprintf(stderr, "Failed to read edid from file\n");

		goto err_mem;
	}

	edid_size = (edid[EDID_NO_EXT_OFFSET] + 1) * EDID_DESC_SIZE;
	if (edid_stat.st_size != edid_size) {
		fprintf(stderr, "EDID file size mismatch: expected %zd, got %jd\n",
				edid_size, (intmax_t)edid_stat.st_size);
		ret = -EFAULT;

		goto err_mem;
	}

	edid_update(edid, mfd);

	for (i = edid[EDID_NO_EXT_OFFSET]; i >= 0; i--) {
		ret = checksum_set(edid + (i * EDID_DESC_SIZE));
		if (ret) {
			fprintf(stderr, "Failed to set checksum(s)\n");

			goto err_mem;
		}
	}

	ret = edid_write(i2c_dev, edid, edid_size);
	if (ret < 0) {
		fprintf(stderr, "Failed to write edid\n");

		goto err_mem;
	}

	ret = edid_verify(i2c_dev, edid, edid_size);
	if (ret < 0) {
		fprintf(stderr, "EDID verification failed\n");

		goto err_mem;
	}

err_mem:
	free(edid);
err_fd:
	close(edid_fd);
err_dev:
	close(i2c_dev);

	return ret;
}

int parse_opts(int argc, char *argv[], char **edid_filename,
	       struct manufacturer_data *mfd, struct eeprom_data *eeprom)
{
	int ret = EXIT_SUCCESS;

	if (argc <= 1) {
		help(argv[0]);

		goto err_arg;
	}

	while (true) {
		unsigned long int retint;
		int c, option_index = 0;
		enum {
		       VERSION_OPT = CHAR_MAX + 1,
		};
		static struct option long_options[] = {
			{ "i2c-bus",	required_argument,	NULL, 'b' },
			{ "chip-addr",	required_argument,	NULL, 'a' },
			{ "prod-code",	required_argument,	NULL, 'p' },
			{ "serial",	required_argument,	NULL, 's' },
			{ "week",	required_argument,	NULL, 'w' },
			{ "year",	required_argument,	NULL, 'y' },
			{ "version",	no_argument,		NULL, VERSION_OPT },
			{ "help",	no_argument,		NULL, 'h' },
			{ NULL,		0,			NULL, 0 }
		};

		c = getopt_long(argc, argv, "a:b:p:s:w:y:h",
				long_options, &option_index);

		if (c == -1) {
			*edid_filename = strdup(argv[optind]);
			if (!edid_filename) {
				fprintf(stderr, "Missing EDID file\n");
				help(argv[0]);
				ret = -EINVAL;

				goto err_arg;
			}
			break;
		}

		switch(c) {
		case 'a':
			retint = strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'chip-addr': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT8_MAX) {
				fputs("'chip-addr' is out of range\n", stderr);
				ret = -ERANGE;
			}
			eeprom->chip_addr = (uint8_t)retint;
			break;
		case 'b':
			retint = strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'i2c-bus': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT8_MAX) {
				fputs("'i2c-bus' is out of range\n", stderr);
				ret = -ERANGE;
			}
			eeprom->i2c_bus = (uint8_t)retint;
			break;
		case 'p':
			retint = strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'prod-code': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT16_MAX) {
				fputs("'prod-code' is out of range\n", stderr);
				ret = -ERANGE;
			}
			mfd->product_code = (uint16_t)retint;
			break;
		case 's':
			retint = strtoul(optarg, NULL, 0);
			strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'serial': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT32_MAX) {
				fputs("'serial' is out of range\n", stderr);
				ret = -ERANGE;
			}
			mfd->serial = (uint32_t)retint;
			break;
		case 'w':
			retint = strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'week': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT8_MAX) {
				fputs("'week' is out of range\n", stderr);
				ret = -ERANGE;
			}
			mfd->week = (uint8_t)retint;
			break;
		case 'y':
			retint = strtoul(optarg, NULL, 0);
			if (errno) {
				fprintf(stderr, "Unable to parse 'year': %s\n",
						strerror(retint));
				ret = -EINVAL;
			}
			if (retint > UINT32_MAX) {
				fputs("'year' is out of range\n", stderr);
				ret = -ERANGE;
			}
			mfd->year = (uint32_t)retint;
			break;
		case VERSION_OPT:
			version(argv[0]);
			ret = EXIT_SUCCESS;

			goto err_arg;
		case 'h':
			help(argv[0]);
			ret = EXIT_SUCCESS;

			goto err_arg;
		case '?': /* fall through */
		default:
			fprintf(stderr, "Invalid arguments supplied\n");
			help(argv[0]);
			ret = -EINVAL;
		}

		if (ret)
			goto err_arg;
	}

	if (!mfd->product_code) {
		fprintf(stderr, "Invalid serial number: 0 < %u\n", mfd->product_code);
		ret = -ERANGE;

		goto err_arg;
	}

	if (!mfd->serial) {
		fprintf(stderr, "Invalid serial number: 0 < %u\n", mfd->serial);
		ret = -ERANGE;

		goto err_arg;
	}

	if (!mfd->week || (mfd->week > WEEKS_PER_YEAR)) {
		fprintf(stderr, "Invalid week of manufacture: 0 < %u <= %u\n",
				mfd->week, WEEKS_PER_YEAR);
		ret = -ERANGE;

		goto err_arg;
	}

	if ((mfd->year < EDID_YEAR_OFFSET) || (mfd->year > EDID_YEAR_MAX)) {
		fprintf(stderr, "Invalid year of manufacture: %u <= %u <= %u\n",
				EDID_YEAR_OFFSET, mfd->year, EDID_YEAR_MAX);
		ret = -ERANGE;

		goto err_arg;
	}

	if (eeprom->i2c_bus > I2C_BUS_COUNT_MAX)
		fprintf(stderr, "Invalid i2c-bus: '%u > %u'\n",
				eeprom->i2c_bus, I2C_BUS_COUNT_MAX);

	if (!eeprom->chip_addr) {
		eeprom->chip_addr = EDID_ADDR;
		fprintf(stderr, "No chip-addr supplied, using default '0x%02x'\n",
				eeprom->chip_addr);
	}

	if (eeprom->chip_addr > EDID_ADDR_MAX) {
		fprintf(stderr, "Out of range chip-addr, '0x%02x > 0x%02x'\n",
				eeprom->chip_addr, EDID_ADDR_MAX);
		ret = -EFAULT;
		help(argv[0]);

		goto err_arg;
	}

	if (eeprom->chip_addr != EDID_ADDR)
		fprintf(stderr, "Chip address is not '0x%02x'\n"
				"Note that valid EDID data must be located at 0x%02x\n",
				EDID_ADDR, EDID_ADDR);

	if (!eeprom->i2c_bus) {
		fprintf(stderr, "Error, missing i2c-bus\n");
		ret = -ENODEV;

		goto err_arg;
	}

err_arg:
	return ret;
}

int main(int argc, char *argv[])
{
	struct manufacturer_data mfd = { 0 };
	struct eeprom_data eeprom = { 0 };
	char *edid_filename = NULL;
	int ret;

	ret = parse_opts(argc, argv, &edid_filename, &mfd, &eeprom);
	if (ret)
		goto err_exit;

	ret = edid_program(edid_filename, &mfd, &eeprom);
	if (ret)
		goto err_exit;

	printf("EDID programmed successfully\n");

err_exit:
	if (edid_filename)
		free(edid_filename);

	return (ret >= 0 ? EXIT_SUCCESS : -ret);
}
